/**  
 * 文 件 名 : ConfigClientEureka
 * 描       述: TODO
 * 作        者： 超哥
 * 创建时间： 2017年3月1日 下午1:25:43
 * 版         本：1.0
 */
package com.qidi.platform.client;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**  
 * 包      名 ：  com.qidi.platform.client
 * 文 件 名 : ConfigClientEureka
 * 描       述: 分布式配置客户端  + enreka 注册
 * 作        者： 超哥
 * 创建时间： 2017年3月1日 下午1:25:43
 * 版         本：1.0
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableAutoConfiguration
public class ConfigClientEureka {

	/**  
	 * 方法名：main
	 * 功能描述: 描述该
	 *
	 * @param args
	 *
	 * 作者： 超哥
	 * 创建时间： 2017年3月1日 下午1:25:43
	 *
	 */
	public static void main(String[] args) {
		
		new SpringApplicationBuilder(ConfigClientEureka.class).web(true).run(args);

	}

}
