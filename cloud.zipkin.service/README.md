#    微服务之分布式跟踪系统ZipKin

------

  zipkin是一个开放源代码分布式的跟踪系统，由Twitter公司开源，它致力于收集服务的定时数据，以解决微服务架构中的延迟问题，包括数据的收集、存储、查找和展现。它的理论模型来自于Google Dapper 论文。
        每个服务向zipkin报告计时数据，zipkin会根据调用关系通过Zipkin UI生成依赖关系图，显示了多少跟踪请求通过每个服务，该系统让开发者可通过一个 Web 前端轻松的收集和分析数据，例如用户每次请求服务的处理时间等，可方便的监测系统中存在的瓶颈。：





### [zipkin下载与启动]()
 在本节中，我们将介绍下载和启动zipkin实例，以便在本地检查zipkin。有三种安装方法：使用官网自己打包好的Jar运行，Docker方式或下载源代码自己打包Jar运行（因为zipkin使用了springboot，内置了服务器，所以可以直接使用jar运行）。zipkin推荐使用docker方式运行，我后面会专门写一遍关于docker的运行方式，而源码运行方式好处是有机会体验到最新的功能特性，但是可能也会带来一些比较诡异的坑，所以不做讲解，下面我直接是使用官网打包的jar运行过程：

    wget -O zipkin.jar  'https://search.maven.org/remote_content?g=io.zipkin.java&a=zipkin-server&v=LATEST&c=exec'  



------
