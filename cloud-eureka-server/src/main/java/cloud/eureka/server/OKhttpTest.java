package cloud.eureka.server;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * @author 超哥
 * @Time：2017年2月15日 下午3:28:07
 * @version 1.0
 */
public class OKhttpTest {
	
	
	public static final MediaType JSON= MediaType.parse("application/json; charset=utf-8");
	static OkHttpClient client = new OkHttpClient();
	
	public static void main(String[] args) throws IOException {
		
		
		

		Request request = new Request.Builder()
	      .url("http://www.baidu.com")
	      .build();

	  Response response = client.newCall(request).execute();
	  
	  
	  
	  	
	  System.out.println(response.body().string());
	  
	  post("http://www.baidu.com","1212");
	  
	  
	  
	  
	}
	
	
	static String post(String url, String json) throws IOException {
		 System.out.println(JSON);
		  RequestBody body = RequestBody.create(JSON, json);
		  Request request = new Request.Builder()
		      .url(url)
		      .post(body)
		      .build();
		  Response response = client.newCall(request).execute();
		  return response.body().string();
	}

}
