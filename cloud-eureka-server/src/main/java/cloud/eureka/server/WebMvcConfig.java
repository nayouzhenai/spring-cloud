package cloud.eureka.server;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.google.code.kaptcha.servlet.KaptchaServlet;

@Configuration  
public class WebMvcConfig extends WebMvcConfigurerAdapter {  
      
    /*@Value("${kaptcha.border}") 
    private String kborder;*/  
      
    @Value("${kaptcha.session.key}")  
    private String skey;  
      
    @Value("${kaptcha.textproducer.font.color}")  
    private String fcolor;  
    
    @Value("${kaptcha.textproducer.font.names}")  
    private String fname;  
    
    
      
    @Value("${kaptcha.textproducer.font.size}")  
    private String fsize;  
      
    @Value("${kaptcha.obscurificator.impl}")  
    private String obscurificator;  
      
    @Value("${kaptcha.noise.impl}")  
    private String noise;  
      
    @Value("${kaptcha.image.width}")  
    private String width;  
      
    @Value("${kaptcha.image.height}")  
    private String height;  
      
    @Value("${kaptcha.textproducer.char.length}")  
    private String clength;  
      
    @Value("${kaptcha.textproducer.char.space}")  
    private String cspace;  
      
    @Value("${kaptcha.background.clear.from}")  
    private String from;  
      
    @Value("${kaptcha.background.clear.to}")  
    private String to;  
      
    
    /***
     * 
     * 方法名：servletRegistrationBean
     * 功能描述:  @Autowired  
     *			private Producer captchaProducer = null;  
     *
     * @return
     * @throws ServletException
     *
     * 作者： 超哥
     * 创建时间： 2016年12月19日 下午2:07:08
     *
     */
    
    @Bean   
    public ServletRegistrationBean servletRegistrationBean() throws ServletException{  
        ServletRegistrationBean servlet = new ServletRegistrationBean(new KaptchaServlet(),"/images/kaptcha.jpg");  
        servlet.addInitParameter("kaptcha.border", "no"/*kborder*/);//无边框  
        servlet.addInitParameter("kaptcha.session.key", skey);//session key  
        servlet.addInitParameter("kaptcha.textproducer.font.color", fcolor);  
        servlet.addInitParameter("kaptcha.textproducer.font.names", fname);  
        servlet.addInitParameter("kaptcha.textproducer.font.size", fsize);  
        servlet.addInitParameter("kaptcha.obscurificator.impl", obscurificator);  
        servlet.addInitParameter("kaptcha.noise.impl", noise);  
        servlet.addInitParameter("kaptcha.image.width", width);  
        servlet.addInitParameter("kaptcha.image.height", height);  
        servlet.addInitParameter("kaptcha.textproducer.char.length", clength);  
        servlet.addInitParameter("kaptcha.textproducer.char.space", cspace);  
        servlet.addInitParameter("kaptcha.background.clear.from", from); //和登录框背景颜色一致   
        servlet.addInitParameter("kaptcha.background.clear.to", to);  
        return servlet;  
    }  
}  