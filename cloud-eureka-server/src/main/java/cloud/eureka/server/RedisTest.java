package cloud.eureka.server;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import redis.clients.jedis.Jedis;

/**
 * redis测试抢购
 * 
 * @author 10255_000
 * 
 */
public class RedisTest {
    public static void main(String[] args) {
        final String watchkeys = "watchkeys";
        int cpuNums = Runtime.getRuntime().availableProcessors();
        System.out.println(cpuNums);
        ExecutorService executor = Executors.newFixedThreadPool(20);

        
        final Jedis jedis =  new Jedis("192.168.0.97", 6379);
        jedis.auth("1063420481");//权限认证
        jedis.set(watchkeys, "0");// 重置watchkeys为0
        jedis.del("setsucc", "setfail");// 清空抢成功的，与没有成功的
        jedis.close();

        for (int i = 0; i < 10000; i++) {// 测试一万人同时访问
            executor.execute(new MyRunnable());
        }
        executor.shutdown();
    }
}